// ==UserScript==
// @name Eli's BC Helper
// @namespace https://www.bondageprojects.com/
// @version 1.01.01
// @description A collection of helpful features for BC
// @author Elicia (Help from Sid)
// @match https://bondageprojects.elementfx.com/*
// @match https://www.bondageprojects.elementfx.com/*
// @match https://bondage-europe.com/*
// @match https://www.bondage-europe.com/*
// @match http://localhost:*/*
// @icon data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant none
// @run-at document-end
// ==/UserScript==

  //SDK stuff

var bcModSdk=function(){"use strict";const o="1.0.2";function e(o){alert("Mod ERROR:\n"+o);const e=new Error(o);throw console.error(e),e}const t=new TextEncoder;function n(o){return!!o&&"object"==typeof o&&!Array.isArray(o)}function r(o){const e=new Set;return o.filter((o=>!e.has(o)&&e.add(o)))}const a=new Map,i=new Set;function d(o){i.has(o)||(i.add(o),console.warn(o))}function c(o,e){if(0===e.size)return o;let t=o.toString().replaceAll("\r\n","\n");for(const[n,r]of e.entries())t.includes(n)||d(`ModSDK: Patching ${o.name}: Patch ${n} not applied`),t=t.replaceAll(n,r);return(0,eval)(`(${t})`)}function s(o){const e=[],t=new Map,n=new Set;for(const r of u.values()){const a=r.patching.get(o.name);if(a){e.push(...a.hooks);for(const[e,i]of a.patches.entries())t.has(e)&&t.get(e)!==i&&d(`ModSDK: Mod '${r.name}' is patching function ${o.name} with same pattern that is already applied by different mod, but with different pattern:\nPattern:\n${e}\nPatch1:\n${t.get(e)||""}\nPatch2:\n${i}`),t.set(e,i),n.add(r.name)}}return e.sort(((o,e)=>e.priority-o.priority)),{hooks:e,patches:t,patchesSources:n,final:c(o.original,t)}}function l(o,e=!1){let r=a.get(o);if(r)e&&(r.precomputed=s(r));else{let e=window;const i=o.split(".");for(let t=0;t<i.length-1;t++)if(e=e[i[t]],!n(e))throw new Error(`ModSDK: Function ${o} to be patched not found; ${i.slice(0,t+1).join(".")} is not object`);const d=e[i[i.length-1]];if("function"!=typeof d)throw new Error(`ModSDK: Function ${o} to be patched not found`);const c=function(o){let e=-1;for(const n of t.encode(o)){let o=255&(e^n);for(let e=0;e<8;e++)o=1&o?-306674912^o>>>1:o>>>1;e=e>>>8^o}return((-1^e)>>>0).toString(16).padStart(8,"0").toUpperCase()}(d.toString().replaceAll("\r\n","\n")),l={name:o,original:d,originalHash:c};r=Object.assign(Object.assign({},l),{precomputed:s(l)}),a.set(o,r),e[i[i.length-1]]=function(o){return function(...e){const t=o.precomputed,n=t.hooks,r=t.final;let a=0;const i=d=>{var c,s,l,f;if(a<n.length){const e=n[a];a++;const t=null===(s=(c=w.errorReporterHooks).hookEnter)||void 0===s?void 0:s.call(c,o.name,e.mod),r=e.hook(d,i);return null==t||t(),r}{const n=null===(f=(l=w.errorReporterHooks).hookChainExit)||void 0===f?void 0:f.call(l,o.name,t.patchesSources),a=r.apply(this,e);return null==n||n(),a}};return i(e)}}(r)}return r}function f(){const o=new Set;for(const e of u.values())for(const t of e.patching.keys())o.add(t);for(const e of a.keys())o.add(e);for(const e of o)l(e,!0)}function p(){const o=new Map;for(const[e,t]of a)o.set(e,{name:e,originalHash:t.originalHash,hookedByMods:r(t.precomputed.hooks.map((o=>o.mod))),patchedByMods:Array.from(t.precomputed.patchesSources)});return o}const u=new Map;function h(o){u.get(o.name)!==o&&e(`Failed to unload mod '${o.name}': Not registered`),u.delete(o.name),o.loaded=!1}function g(o,t,r){"string"==typeof o&&o||e("Failed to register mod: Expected non-empty name string, got "+typeof o),"string"!=typeof t&&e(`Failed to register mod '${o}': Expected version string, got ${typeof t}`),r=!0===r;const a=u.get(o);a&&(a.allowReplace&&r||e(`Refusing to load mod '${o}': it is already loaded and doesn't allow being replaced.\nWas the mod loaded multiple times?`),h(a));const i=t=>{"string"==typeof t&&t||e(`Mod '${o}' failed to patch a function: Expected function name string, got ${typeof t}`);let n=c.patching.get(t);return n||(n={hooks:[],patches:new Map},c.patching.set(t,n)),n},d={unload:()=>h(c),hookFunction:(t,n,r)=>{c.loaded||e(`Mod '${c.name}' attempted to call SDK function after being unloaded`);const a=i(t);"number"!=typeof n&&e(`Mod '${o}' failed to hook function '${t}': Expected priority number, got ${typeof n}`),"function"!=typeof r&&e(`Mod '${o}' failed to hook function '${t}': Expected hook function, got ${typeof r}`);const d={mod:c.name,priority:n,hook:r};return a.hooks.push(d),f(),()=>{const o=a.hooks.indexOf(d);o>=0&&(a.hooks.splice(o,1),f())}},patchFunction:(t,r)=>{c.loaded||e(`Mod '${c.name}' attempted to call SDK function after being unloaded`);const a=i(t);n(r)||e(`Mod '${o}' failed to patch function '${t}': Expected patches object, got ${typeof r}`);for(const[n,i]of Object.entries(r))"string"==typeof i?a.patches.set(n,i):null===i?a.patches.delete(n):e(`Mod '${o}' failed to patch function '${t}': Invalid format of patch '${n}'`);f()},removePatches:o=>{c.loaded||e(`Mod '${c.name}' attempted to call SDK function after being unloaded`);i(o).patches.clear(),f()},callOriginal:(t,n,r)=>(c.loaded||e(`Mod '${c.name}' attempted to call SDK function after being unloaded`),"string"==typeof t&&t||e(`Mod '${o}' failed to call a function: Expected function name string, got ${typeof t}`),Array.isArray(n)||e(`Mod '${o}' failed to call a function: Expected args array, got ${typeof n}`),function(o,e,t=window){return l(o).original.apply(t,e)}(t,n,r)),getOriginalHash:t=>("string"==typeof t&&t||e(`Mod '${o}' failed to get hash: Expected function name string, got ${typeof t}`),l(t).originalHash)},c={name:o,version:t,allowReplace:r,api:d,loaded:!0,patching:new Map};return u.set(o,c),Object.freeze(d)}function m(){const o=[];for(const e of u.values())o.push({name:e.name,version:e.version});return o}let w;const y=void 0===window.bcModSdk?window.bcModSdk=function(){const e={version:o,apiVersion:1,registerMod:g,getModsInfo:m,getPatchingInfo:p,errorReporterHooks:Object.seal({hookEnter:null,hookChainExit:null})};return w=e,Object.freeze(e)}():(n(window.bcModSdk)||e("Failed to init Mod SDK: Name already in use"),1!==window.bcModSdk.apiVersion&&e(`Failed to init Mod SDK: Different version already loaded ('1.0.2' vs '${window.bcModSdk.version}')`),window.bcModSdk.version!==o&&alert(`Mod SDK warning: Loading different but compatible versions ('1.0.2' vs '${window.bcModSdk.version}')\nOne of mods you are using is using an old version of SDK. It will work for now but please inform author to update`),window.bcModSdk);return"undefined"!=typeof exports&&(Object.defineProperty(exports,"__esModule",{value:!0}),exports.default=y),y}();
//SDKstuff end



(async function () {
  const ver = "1.01.01";
  var latestupdate = "EBCH updated (" + ver + "):\n" + 
  "-Refactored the chat commands for consistency with BC commands.\n" + 
  "-Reorganized some of the code so it makes more sense.\n" + 
  "-Added a toggle for the welcome message.\n" + 
  "-Added a toggle for turning on/off the rendering of the small pose button.\n" + 
  "-Lots of random fixes and optimizations.";
  const modApi = bcModSdk.registerMod('EBCH', ver);
  const ManualUrl = "https://e2466.gitlab.io/ebch/";
  const WelcomeString = "EBCH: /ebch for commands.\n<a href=" + ManualUrl + " target=\"_blank\">Online Guide</a>";
  const posearray = ["Yoked","BaseLower","BaseUpper","KneelingSpread","Kneel","OverTheHead","Hogtied","AllFours","BackBoxTie","LegsClosed","Spread","BackElbowTouch","LegsOpen"];
  const posecdadd = 2000;
  const generalhelp = "EBCH Commands:\n" +
                "<i>/ebch</i> or <i>/ebch help</i>: Diplays this help menu.\n" +
                "<i>/ebch reset</i>: Resets EBCH settings to default.\n" +
                "<i>/ebch welcome</i>: Toggles welcome message on and off.\n" +
                "<i>/ebch log</i>: Displays chatlogging related help.\n" +
                "<i>/ebch notifs</i>: Displays custom notifications related help.\n" +
                "<i>/ebch ungarble</i>: Displays ungarble related help.\n" +
                "<i>/ebch pose</i>: Displays pose related help.\n" +
                "<a href=" + ManualUrl + " target=\"_blank\">Online Guide</a>";
  const posehelp = "EBCH Pose Commands:\n" +
                  "<i>/ebch pose help</i>: Diplays this help menu.\n" +
                  "<i>/ebch pose on/off</i>: Turns on or off the rendering of the Pose UI button.\n" +
                  "<i>/ebch pose [posegoeshere] [targetgoeshere]</i>: Changes the target to the written pose.\n" +
                  "Pose options: Yoked, BaseLower, BaseUpper, KneelingSpread, Kneel, OverTheHead, Hogtied, AllFours, BackBoxTie, LegsClosed, Spread, BackElbowTouch, LegsOpen.\n" +
                  "<a href=" + ManualUrl + " target=\"_blank\">Online Guide</a>";
  const ungarblehelp = "Please note that some addons do use alternate garbling methods, and if those are turned on on the target's side, this won't ungarble them.\nBCE is a good example of this.\n\n" +
                  "EBCH Ungarble Commands:\n" +
                  "<i>/ebch ungarble help</i>: Diplays this help menu.\n" +
                  "<i>/ebch ungarble off</i>: Turns off the EBCH ungarble.\n" +
                  "<i>/ebch ungarble hw</i>: Turns on the EBCH ungarble, but only for those on your hearing whitelist.\n" +
                  "<i>/ebch ungarble all</i>: Turns on the EBCH ungarble for everyone.\n" +
                  "<i>/ebch ungarble add</i>: Adds someone <i>in your chatroom</i> to the hearing whitelist. Works with name and number.\n" +
                  "<i>/ebch ungarble rem</i>: Removes someone from the hearing whitelist. Only works with player number.\n" +
                  "<i>/ebch ungarble list</i>: Lists the player numbers on your hearing whitelist.\n" +
                  "<i>/ebch ungarble clear</i>: Clears your hearing whitelist.\n" +
                  "<a href=" + ManualUrl + " target=\"_blank\">Online Guide</a>";
  const loghelp = "EBCH logging Commands:\n" +
                  "<i>/ebch log help</i>: Diplays this help menu.\n" +
                  "<i>/ebch log on/off</i>: Turns Chatlogging on/off.\n" +
                  "<i>/ebch log clear</i>: Clears chatlogs associated with the current character.\n" +
                  "<i>/ebch log download</i>: Creates a log from the database and sends it as a downloadable file.\n" +
                  "<a href=" + ManualUrl + " target=\"_blank\">Online Guide</a>";
  const notifshelp = "Please note that message notifications in BC options need to be enabled for this to work.\n\n" +
                  "EBCH Custom Notifications Commands:\n" +
                  "<i>/ebch notifs help</i>: Diplays this help menu.\n" +
                  "<i>/ebch notifs on/off</i>: Turns notifications on or off.\n" +
                  "<i>/ebch notifs add</i>: Adds a word to the notification words list.\n" +
                  "<i>/ebch notifs rem</i>: Removes a word from the notification words list.\n" +
                  "<i>/ebch notifs clear</i>: Clears the notification words list.\n" +
                  "<i>/ebch notifs list</i>: Lists the words that will trigger a notification.\n" +
                  "<a href=" + ManualUrl + " target=\"_blank\">Online Guide</a>";

  //buttons loc
  // x, y, size, spacing (anchor self menu)
  const selfmenuanchor = [20, 780, 40, 10];
  // x  , y , size x, size y, spacing (anchor pose ui)
  const puicoords = [300, 10, 100, 40, 5];

  const buttungarble = [selfmenuanchor[0], selfmenuanchor[1], selfmenuanchor[2], selfmenuanchor[2]];
  const buttnotifs = [selfmenuanchor[0] + selfmenuanchor[2] + selfmenuanchor[3], selfmenuanchor[1], selfmenuanchor[2], selfmenuanchor[2]];
  const buttlog = [selfmenuanchor[0], selfmenuanchor[1] + selfmenuanchor[2] + selfmenuanchor[3], selfmenuanchor[2], selfmenuanchor[2]];

  const buttposeui = [puicoords[0] + 50,puicoords[1],40,40];
  const buttbaseupper = [puicoords[0] + puicoords[2] + puicoords[4],puicoords[1],puicoords[2],puicoords[3]];
  const buttyoked = [puicoords[0] + (puicoords[2] + puicoords[4]),puicoords[1]  + (puicoords[3] + puicoords[4]),puicoords[2],puicoords[3]];
  const buttoverthehead = [puicoords[0] + (puicoords[2] + puicoords[4]),puicoords[1] + (puicoords[3] + puicoords[4]) * 2,puicoords[2],puicoords[3]];
  const buttbackboxtie = [puicoords[0] + (puicoords[2] + puicoords[4]),puicoords[1] + (puicoords[3] + puicoords[4]) * 3,puicoords[2],puicoords[3]];
  const buttbackelbowtouch = [puicoords[0] + (puicoords[2] + puicoords[4]),puicoords[1] + (puicoords[3] + puicoords[4]) * 4,puicoords[2],puicoords[3]];
  const buttbaselower = [puicoords[0] + (puicoords[2] + puicoords[4]),puicoords[1] + (puicoords[3] + puicoords[4]) * 6,puicoords[2],puicoords[3]];
  const buttkneel = [puicoords[0] + (puicoords[2] + puicoords[4]),puicoords[1] + (puicoords[3] + puicoords[4]) * 7,puicoords[2],puicoords[3]];
  const buttkneelspread = [puicoords[0] + (puicoords[2] + puicoords[4]),puicoords[1] + (puicoords[3] + puicoords[4]) * 8,puicoords[2],puicoords[3]];
  const buttlegsclosed = [puicoords[0] + (puicoords[2] + puicoords[4]),puicoords[1] + (puicoords[3] + puicoords[4]) * 9,puicoords[2],puicoords[3]];
  const buttspread = [puicoords[0] + (puicoords[2] + puicoords[4]),puicoords[1] + (puicoords[3] + puicoords[4]) * 10,puicoords[2],puicoords[3]];
  const butthogtied = [puicoords[0] + (puicoords[2] + puicoords[4]),puicoords[1] + (puicoords[3] + puicoords[4]) * 12,puicoords[2],puicoords[3]];
  const buttallfours = [puicoords[0] + (puicoords[2] + puicoords[4]),puicoords[1] + (puicoords[3] + puicoords[4]) * 13,puicoords[2],puicoords[3]];

  //-            
  var HearingWhitelist = [];
  var notifwords = [];
  var ungarble = 0;
  antiGarbling();
  crCommands();
  poseMenuOthers();
  poseMenuOthersClick();
  ebchLogging();
  ebchwhnet();
  var debug = 0;
  var focus = 1;
  var logging = 0;
  var notifs = 0;
  var db;
  var dbsetup = 0;
  var textToWrite;
  var poseui = 0;
  var poseuirender = 1;
  var lastmsg;

                      
  var posecd = Date.now();
  var welcomemsg = 0;
  var welcomemsgtoggle = 1;

  if (CurrentScreen === "ChatRoom" && Player.OnlineSettings.EBCH !== "" || CurrentScreen === "ChatRoom" && Player.OnlineSettings.EBCH !== undefined) {Load();}
  if (welcomemsgtoggle === 1 && welcomemsg === 0 && CurrentScreen === "ChatRoom")
  {
    welcomemsg = 1;
    ChatRoomSendLocal(WelcomeString);
    
  }

  //dbfunctions

  function openDb(DB_NAME, DB_STORE_NAME) {
    console.log("EBCH: Preparing DB.");
    var req = indexedDB.open(DB_NAME, 1);
    req.onsuccess = function (evt) {
      // Equal to: db = req.result;
      db = this.result;
      console.log("EBCH: DB Ready.");
      dbsetup = 1;
    };
    req.onerror = function (evt) {
      console.error("openDb:", evt.target.errorCode);
    };

    req.onupgradeneeded = function (evt) {
      console.log("EBCH: DB Update Process.");
      var store = evt.currentTarget.result.createObjectStore(
        DB_STORE_NAME, { keyPath: 'id', autoIncrement: true });
    };
  }

  function adddata(text, DB_STORE_NAME) {
    if (debug === 1) {console.log("add database.", arguments);}
    const datenow = new Date(Date.now());
    text = text.replaceAll("\n", "<br>");
    var obj = { logmsg: "[" + datenow.toLocaleDateString() + " - " + datenow.toLocaleTimeString() + " - " + Player.LastChatRoom + "] " + text } ;
    if (typeof blob != 'undefined')
      obj.blob = blob;
    var store = getObjectStore(DB_STORE_NAME, 'readwrite');
    var req;
    try {
      req = store.add(obj);
    } catch (e) {
      if (e.name == 'DataCloneError')
      throw e;
    }
    req.onsuccess = function (evt) {
      if(debug === 1) {console.log("EBCH: DB Insertion worked.");}
    };
    req.onerror = function() {
      if(debug === 1) {console.log("EBCH: DB Insertion error.");}
    };
  }

  function getObjectStore(store_name, mode) {
    var tx = db.transaction(store_name, mode);
    return tx.objectStore(store_name);
  }

  function clearObjectStore(DB_STORE_NAME) {
    var store = getObjectStore(DB_STORE_NAME, 'readwrite');
    var req = store.clear();
    req.onsuccess = function(evt) {
      ChatRoomSendLocal("EBCH: Database " + DB_STORE_NAME + " cleared successfully.");
    };
    req.onerror = function (evt) {
      console.error("clearObjectStore:", evt.target.errorCode);
    };
  }

  function retrieveAll(DB_STORE_NAME) {
      var store = getObjectStore(DB_STORE_NAME, 'readwrite');
      var msgs = [];
      store.openCursor().onsuccess = async function(event) {
      var cursor = event.target.result;
      if (cursor) {
      msgs = msgs + Object.values(cursor.value)[0];
      cursor.continue();
      } else {
    textToWrite = msgs;
    ChatRoomSendLocal("EBCH: Data Retrieved. Preparing and formatting text file...");
    saveTextAsFileCont();
      }
    };

  }

    function saveTextAsFile()
    {
      var store = "logs" + JSON.stringify(Player.MemberNumber);
      ChatRoomSendLocal("EBCH: Retrieving data from DB. This may take a while.");
      retrieveAll(store);
    }

  function saveTextAsFileCont() {
    if(textToWrite === "") {return ChatRoomSendLocal("EBCH: No content to output, exiting export function.");}
    //textToWrite = textToWrite.replaceAll('"',"");
    var textFileAsBlob = new Blob([textToWrite], {type:'text/html'});
    const datenow = new Date(Date.now());
    var fileNameToSaveAs = "BC ChatLog " + JSON.stringify(Player.Name) + " - " + datenow.toLocaleDateString() + " - " + datenow.toLocaleTimeString();
    //if we are on gecko, add .html extension (1.10.00)
    if (!window.webkitURL != null)
    {
      fileNameToSaveAs = fileNameToSaveAs + ".html";
    }
    var downloadLink = document.createElement("a");
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innerHTML = "Download File";
    ChatRoomSendLocal("EBCH: Sending file.");
    if (window.webkitURL != null)
    {
        // Chrome allows the link to be clicked
        // without actually adding it to the DOM.
        downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
    }
    else
    {
        // Firefox requires the link to be added to the DOM
        // before it can be clicked.
        downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
        downloadLink.onclick = destroyClickedElement;
        downloadLink.style.display = "none";
        document.body.appendChild(downloadLink);
    }

    downloadLink.click();
    textToWrite = "";
  }

  //end of db functions
  //check if the page is in focus or not
  document.addEventListener('visibilitychange', function (event) {
    if (document.hidden) {
        focus = 0;
    } else {
        focus = 1;
    }
  });


  await waitFor(() => ServerIsConnected && ServerSocket);

  function targetfind(input) {
    var targetnumber = parseInt(input);
    var target;
    if(targetnumber >= 0)
    {
      target = ChatRoomCharacter.find((x) => x.MemberNumber === targetnumber);
    } else {
      target = ChatRoomCharacter.find((x) => x.Name.toLowerCase() === input.trim());
    }
    return target;
  }

  function Act(P, AG, AN) {
    if(debug === 1) console.log("Trying to boop ", P);
    // ensure activity is allowed and is not being done on player
    if (!ActivityAllowedForGroup(P, AG).some(a => a.Name == AN)) return;
    var Dictionary = [];
    Dictionary.push({ Tag: "SourceCharacter", Text: Player.Name, MemberNumber: Player.MemberNumber });
    Dictionary.push({ Tag: "TargetCharacter", Text: P.Name, MemberNumber: P.MemberNumber });
    Dictionary.push({ Tag: "ActivityGroup", Text: AG });
    Dictionary.push({ Tag: "ActivityName", Text: AN });
    ServerSend("ChatRoomChat", { Content: ((P.ID == 0) ? "ChatSelf-" : "ChatOther-") + AG + "-" + AN, Type: "Activity", Dictionary: Dictionary });
  }

  function ActionBeep(tar, str1,str2){
    ServerSend("ChatRoomChat", { Content: "Beep", Type: "Action", Dictionary: [{Tag: "Beep", Text: Player.Name + str1 + tar.Name + str2}]});
  }

  function Pose(tar, pose){
    const currenttime = Date.now();
    if(Player.Effect.indexOf("Block") === -1 && posecd <= currenttime && CharacterCanChangeToPose(tar, pose) && !tar.BlackList.includes(Player.MemberNumber) && (tar.ItemPermission <=2 || tar.ID == 0 || tar.IsLoverOfPlayer() || tar.IsOwnedByPlayer() || tar.WhiteList.includes(Player.MemberNumber))){
      CharacterSetActivePose(tar, pose);
      ChatRoomCharacterUpdate(tar);
    if(tar.MemberNumber === Player.MemberNumber)
    {
      return;
    } else {
        posecd = currenttime + posecdadd;
        switch (pose){
          case "Yoked":
            ActionBeep(tar," raises ", "'s hands.");
            break;

          case "BaseLower":
            ActionBeep(tar," helps ", " up on their feet.");
            break;

          case "BaseUpper":
            ActionBeep(tar," lets ", " relax their arms.");
            break;

          case "KneelingSpread":
            ActionBeep(tar," lowers ", " on their knees, forcing their legs open.");
            break;

          case "Kneel":
            ActionBeep(tar," helps ", " to their knees.");
            break;

          case "OverTheHead":
            ActionBeep(tar," forcibly raises ", "'s hands above their head.");
            break;

          case "HogTied":
            ActionBeep(tar," lowers ", " on their belly.");
            break;

          case "AllFours":
            ActionBeep(tar," forces ", " on all fours.");
            break;

          case "BackBoxTie":
            ActionBeep(tar," draws ", "'s arms behind their back.");
            break;

          case "LegsClosed":
            ActionBeep(tar," helps ", " stand straight with their legs closed.");
            break;

          case "Spread":
            ActionBeep(tar," forces ", " to spread their legs.");
            break;

          case "BackElbowTouch":
            ActionBeep(tar," draws ", "'s arms behind their back, elbows almost touching.");
            break;

          case "LegsOpen":
            ActionBeep(tar," helps ", " stand straight with their legs open.");
            break;

          default:
            break;

        }
        return;
      }
    }
  }

  function Save() {
    //debug,antigarble
    if(debug === 1) {console.log("saving settings");}
    var sdebug;
    var sungarble;
    var slogging;
    var sposeui;
    var sHW;
    var sNW;
    var sN;
    var sposeuirender;
    var swelcomemsgtoggle;
    var ebchsettings;
    if(JSON.stringify(debug) === null)
    {
      sdebug = "0";
    } else {
      sdebug = JSON.stringify(debug);
    }
    if(JSON.stringify(ungarble) === null)
    {
      sungarble = "0";
    } else {
      sungarble = JSON.stringify(ungarble);
    }
    if(JSON.stringify(logging) === null)
    {
      slogging = "0";
    } else {
      slogging = JSON.stringify(logging);
    }
    if(JSON.stringify(notifs) === null)
    {
      sN = "0";
    } else {
      sN = JSON.stringify(notifs);
    }
    if(JSON.stringify(poseui) === null)
    {
      sposeui = "0";
    } else {
      sposeui = JSON.stringify(poseui);
    }
    if(JSON.stringify(poseuirender) === null)
    {
      sposeuirender = "0";
    } else {
      sposeuirender = JSON.stringify(poseuirender);
    }
    if(JSON.stringify(welcomemsgtoggle) === null)
    {
      swelcomemsgtoggle = "0";
    } else {
      swelcomemsgtoggle = JSON.stringify(welcomemsgtoggle);
    }
    if(JSON.stringify(HearingWhitelist) === null) {
      sHW = "";
    } else {
      sHW = JSON.stringify(HearingWhitelist);
    }
    if(JSON.stringify(notifwords) === null) {
      sNW = "";
    } else {
      sNW = JSON.stringify(notifwords);
    }

    ebchsettings =  sdebug + "," + sungarble + "," + slogging + "," + sposeui + "," + sN +  "," + ver + "," + sposeuirender + "," + swelcomemsgtoggle + "|" + sHW + "|" + sNW;
    ebchsettings = ebchsettings.replaceAll("[","");
    ebchsettings = ebchsettings.replaceAll("]","");
    Player.OnlineSettings.EBCH = ebchsettings;
    ServerAccountUpdate.QueueData({ OnlineSettings: Player.OnlineSettings })
  }

  function Load() {
    if(Player.OnlineSettings.EBCH !== undefined) {
    if(debug === 1) {console.log("loading settings");}
      var ebchsettings = Player.OnlineSettings.EBCH;
      ebchsettings = ebchsettings.replace("/g","");
      ebchsettings = ebchsettings.replaceAll('"',"");
      console.log(ebchsettings);
      if(ebchsettings !== ""){
        var setlist = ebchsettings.split("|");
        var wl = setlist[1];
        if(wl !== "" && wl !== undefined) {
          HearingWhitelist = wl.split(",");
          HearingWhitelist = HearingWhitelist.map((x) => +x);
        } else {
          HearingWhitelist = [];
        }
        var nw = setlist[2];
        if(nw !== "" && nw !== undefined) {
          notifwords = nw.split(",");
        } else {
          notifwords = [];
        }
        var settings = setlist[0].split(",");
        debug = parseInt(settings[0]);
        ungarble = parseInt(settings[1]);
        logging = parseInt(settings[2]);
        if(logging === 1 && dbsetup === 0){openDb("BCLogs"+ JSON.stringify(Player.MemberNumber), "logs" + JSON.stringify(Player.MemberNumber));}
        notifs = parseInt(settings[4]);
        poseui = parseInt(settings[3]);
        if(settings[6])
        {
          poseuirender = parseInt(settings[6]);
        } else {
          poseuirender = 1;
        }
        if(settings[7])
        {
          welcomemsgtoggle = parseInt(settings[7]);
        } else {
          welcomemsgtoggle = 1;
        }
        
        if(settings[5] !== ver) {
          ChatRoomSendLocal(latestupdate);
          Save();
        }
      }
    }
  }

  function SelfMenuDraw() {
    if(ungarble === 0)
    {
      DrawButton(...buttungarble, "Ung", "White", "", "EBCH: Turn on Ungarble (Hearing Whitelist)");
    } else if(ungarble === 1)
    {
      DrawButton(...buttungarble, "Ung", "Yellow", "", "EBCH: Turn on Ungarble (all)");
    } else if(ungarble === 2) {
      DrawButton(...buttungarble, "Ung", "Green", "", "EBCH: Turn off ungarble");
    }
    if(notifs === 0)
    {
      DrawButton(...buttnotifs, "Not", "White", "", "EBCH: Turn on custom notifications");
    } else if(notifs === 1) {
      DrawButton(...buttnotifs, "Not", "Green", "", "EBCH: Turn off custom notifications");
    }
    if(logging === 0)
    {
      DrawButton(...buttlog, "Log", "White", "", "EBCH: Turn on chatlogging");
    } else if (logging === 1) {
      DrawButton(...buttlog, "Log", "Green", "", "EBCH: Turn off chatlogging");
    }

    
  }

  function PoseMenuDraw() {
    if(poseuirender === 1 && poseui === 1) {
      var target = CurrentCharacter;
      // x  , y , size x, size y, spacing
      if(CharacterCanChangeToPose(target, "BaseUpper")) {
        DrawButton(...buttbaseupper,"BaseHand","White","","Base Hands");
      } else {
        DrawButton(...buttbaseupper,"BaseHand","Red","","Base Hands");
      }
      if(CharacterCanChangeToPose(target, "Yoked")) {
        DrawButton(...buttyoked, "HandsUp", "White","","Hands Up");
      } else {
        DrawButton(...buttyoked, "HandsUp", "Red","","Hands Up");
      }
      if(CharacterCanChangeToPose(target, "OverTheHead")) {
        DrawButton(...buttoverthehead, "HandsHigh", "White","","Hands Up High");
      } else {
        DrawButton(...buttoverthehead, "HandsHigh", "Red","","Hands Up High");
      }
      if(CharacterCanChangeToPose(target, "BackBoxTie")) {
        DrawButton(...buttbackboxtie, "BackLoose", "White","","Back Loose");
      } else {
        DrawButton(...buttbackboxtie, "BackLoose", "Red","","Back Loose");
      }
      if(CharacterCanChangeToPose(target, "BackElbowTouch")) {
        DrawButton(...buttbackelbowtouch, "BackTight", "White","","Back Tight");
      } else {
        DrawButton(...buttbackelbowtouch, "BackTight", "Red","","Back Tight");
      }
      if(CharacterCanChangeToPose(target, "BaseLower")) {
        DrawButton(...buttbaselower,"Standing","White","","Stand");
      } else {
        DrawButton(...buttbaselower,"Standing","Red","","Stand");
      }
      if(CharacterCanChangeToPose(target, "Kneel")) {
        DrawButton(...buttkneel,"Kneeling","White","","Kneel");
      } else {
        DrawButton(...buttkneel,"Kneeling","Red","","Kneel");
      }
      if(CharacterCanChangeToPose(target, "KneelingSpread")) {
        DrawButton(...buttkneelspread, "KneelSpr","White","","Kneel Spread");
      } else {
        DrawButton(...buttkneelspread,"KneelSpr","Red","","Kneel Spread");
      }
      if(CharacterCanChangeToPose(target, "LegsClosed")) {
        DrawButton(...buttlegsclosed,"StandCl","White","","Standing Closed Legs");
      } else {
        DrawButton(...buttlegsclosed,"StandCl","Red","","Standing Closed Legs");
      }
      if(CharacterCanChangeToPose(target, "Spread")) {
        DrawButton(...buttspread,"StandSpr","White","","Standing Spread");
      } else {
        DrawButton(...buttspread,"StandSpr","Red","","Standing Spread");
      }
      if(CharacterCanChangeToPose(target, "Hogtied")) {
        DrawButton(...butthogtied, "BellyLie", "White","","Belly Lie");
      } else {
        DrawButton(...butthogtied, "BellyLie", "Red","","Belly Lie");
      }
      if(CharacterCanChangeToPose(target, "AllFours")) {
        DrawButton(...buttallfours,"AllFours","White","","All Fours");
      } else {
        DrawButton(...buttallfours,"AllFours","Red","","All Fours");
      }
    }
  }

  function ebchlogadd() {
    var message = Array.from(document.getElementsByClassName('ChatMessage')).slice(-1)[0].textContent;
        if(dbsetup === 1 && lastmsg !== message) {
          lastmsg = message;
          adddata(Array.from(document.getElementsByClassName('ChatMessage')).slice(-1)[0].outerHTML, "logs" + JSON.stringify(Player.MemberNumber));
        }

  }

    // users in the chatroom are stored in ChatRoomCharacter array
  // on channel join data Type is Action, Content is ServerEnter and MemberNumber is the joining user
  ServerSocket.on("ChatRoomMessage", async (data) => {

    if(debug === 1) console.log("ChatRoomMessageBit", data);

    // if the data is not a ServerEnter, return
    //if (data.Content !== "ServerEnter" && data.Type !== "Chat" && data.Type !== "Action" && data.Type !== "Activity" && data.Type !== "Emote" && data.Type !== "Whisper") {
      //return;
    //}
    //load settings when entering chatroom
    if(data.Content === "ServerEnter" && data.Sender === Player.MemberNumber) {
      Load();
      if(welcomemsgtoggle === 1 && welcomemsg === 0){
        welcomemsg = 1;
        ChatRoomSendLocal(WelcomeString);
      }
      
      if(dbsetup === 0) {openDb("BCLogs"+ JSON.stringify(Player.MemberNumber), "logs" + JSON.stringify(Player.MemberNumber));}
    }
    if(data.Type === "Chat" && focus === 0 && notifs === 1) {
      for (const P of notifwords) {
        var cont = data.Content.toLowerCase();
        let index = cont.search(P);
        if(index !== -1) {
          //word was found
          if(data.Content.length === P.length || index === 0 && data.Content.substring(P.length ,P.length + 1) === " " || index !== 0 && index !== -1 && data.Content.substring(index - 1, index) === " " && (data.Content.substring(index + P.length,index + P.length + 1) === " " || data.Content.length === index + P.length))
          NotificationRaise("ChatMessage", data.Content);
        }

      }
    }
    if(data.Type === "Whisper" && logging === 1) {
      ebchlogadd();

    }


    return;
  });



  async function waitFor(func, cancelFunc = () => false) {
    while (!func()) {
      if (cancelFunc()) {
        if(debug === 1) console.log("waitFor returning false.");
        return false;
      }
      if(debug === 1) console.log("waitFor sleep bit.");
      await sleep(100);
    }
    if(debug === 1) console.log("waitFor returning true.");
    return true;
  }

  function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }
  //catches the garble function to handle the antigarble
  async function antiGarbling() {
      await waitFor(() => !!SpeechGarble);
      modApi.hookFunction('SpeechGarble', 4, (args, next) => {
        // Copy original, which is second argument
        const originalText = args[1];
        const source = args[0];
        // Call the original function, saving result
        const garbledText = next(args);
        // Return modified result by adding original text after the garbled text
        if(!originalText)
        {
          return "";
        }
        if(originalText.indexOf("(") !== 0 &&  SpeechGetTotalGagLevel(source) >= 1 && ungarble !== 0 && (HearingWhitelist.includes(source.MemberNumber) || ungarble === 2 || HearingWhitelist == source.MemberNumber))
        {
          return originalText + " (Ungarbled)";
        } else if(originalText.indexOf("(") === 0 &&  SpeechGetTotalGagLevel(source) >= 1 && ungarble !== 0 && (HearingWhitelist.includes(source.MemberNumber) || ungarble === 2 || HearingWhitelist == source.MemberNumber)){
          return originalText;
        } else {
          return garbledText;
        }
        //return garbledText + ' <> ' + originalText;
      });
    }
  //catches server sends to trigger chatlogging >//< only way I found to fix the fact sending whispers didn't trigger the chatlogging
  async function ebchwhnet(){
    await waitFor(() => !!ServerSend)
    modApi.hookFunction('ServerSend', 4, (args, next) => {
      next(args);
      if(CurrentScreen === "ChatRoom" && Array.from(document.getElementsByClassName('ChatMessage')).slice(-1)[0]) {
        ebchlogadd();
        return;         
        
      }

      
    })
  }
  //catches dialogclick to handle pose ui
  async function poseMenuOthersClick() {
    await waitFor(() => DialogClick);
    modApi.hookFunction("DialogClick", 4, (args,next) => {
      if(CurrentCharacter.ID === 0) {
        //ungarble button
        if(MouseIn(...buttungarble)) {
          if(ungarble === 0) {
            ungarble = 1;
            //ChatroomSendLocal("EBCH: Ungarble: Hearing Whitelist turned on.\n Type !ebchungarblehelp for commands.");
            ClearButtons();
            DialogDraw();
            Save();
          } else if(ungarble === 1) {
            ungarble = 2;
            //ChatroomSendLocal("EBCH: Ungarble all turned on.\n Type !ebchungarblehelp for commands.");
            ClearButtons();
            DialogDraw();
            Save();
          } else if(ungarble === 2) {
            ungarble = 0;
            //ChatroomSendLocal("EBCH: Ungarble turned off.");
            ClearButtons();
            DialogDraw();
            Save();
          }
        } else if (MouseIn(...buttnotifs)) {
          if(notifs === 0){
            notifs = 1;
            //ChatroomSendLocal("EBCH: Custom notifications turned on.\nType !ebchnotifhelp for commands.");
            ClearButtons();
            DialogDraw();
            Save();
          } else if (notifs === 1){
            notifs = 0;
            //ChatroomSendLocal("EBCH: Custom notifications turned off.");
            ClearButtons();
            DialogDraw();
            Save();
          }
        } else if (MouseIn(...buttlog)) {
          if(logging === 0) {
            logging = 1;
            if(dbsetup === 0)  {openDb("BCLogs"+ JSON.stringify(Player.MemberNumber), "logs" + JSON.stringify(Player.MemberNumber));}
            //ChatroomSendLocal("EBCH: Chatlogging turned on.\nType !ebchlogginghelp for commands.");
            ClearButtons();
            DialogDraw();
            Save();
          } else if (logging === 1) {
            logging = 0;
            //ChatroomSendLocal("EBCH: Chatlogging turned off.\n");
            ClearButtons();
            DialogDraw();
            Save();
          }
        }
      }
      if(poseuirender === 1 && poseui === 1 && MouseIn(...buttposeui) && CurrentCharacter.ID !== 0)
      {
        poseui = 0;
        ClearButtons();
        DialogDraw();
        Save();
        
      }
      else if(poseuirender === 1 && poseui === 0 && MouseIn(...buttposeui) && CurrentCharacter.ID !== 0)
      {
        poseui = 1;
        ClearButtons();
        DialogDraw();
        Save();
      }
      
      if(poseuirender === 1 && poseui === 1) {
          if (MouseIn(...buttbaseupper) && CurrentCharacter.ID !== 0 && CharacterCanChangeToPose(CurrentCharacter, "BaseUpper")) {
          Pose(CurrentCharacter, "BaseUpper");
          return;
        }
        if (MouseIn(...buttyoked) && CurrentCharacter.ID !== 0 && CharacterCanChangeToPose(CurrentCharacter, "Yoked")) {
          Pose(CurrentCharacter, "Yoked");
          return;
        }
        if (MouseIn(...buttoverthehead) && CurrentCharacter.ID !== 0 && CharacterCanChangeToPose(CurrentCharacter, "OverTheHead")) {
          Pose(CurrentCharacter, "OverTheHead");
          return;
        }
        if (MouseIn(...buttbackboxtie) && CurrentCharacter.ID !== 0 && CharacterCanChangeToPose(CurrentCharacter, "BackBoxTie")) {
          Pose(CurrentCharacter, "BackBoxTie");
          return;
        }
        if (MouseIn(...buttbackelbowtouch) && CurrentCharacter.ID !== 0 && CharacterCanChangeToPose(CurrentCharacter, "BackElbowTouch")) {
          Pose(CurrentCharacter, "BackElbowTouch");
          return;
        }
        if (MouseIn(...buttbaselower) && CurrentCharacter.ID !== 0 && CharacterCanChangeToPose(CurrentCharacter, "BaseLower")) {
          Pose(CurrentCharacter, "BaseLower");
          return;
        }
        if (MouseIn(...buttkneel) && CurrentCharacter.ID !== 0 && CharacterCanChangeToPose(CurrentCharacter, "Kneel")) {
          Pose(CurrentCharacter, "Kneel");
          return;
        }
        if (MouseIn(...buttkneelspread) && CurrentCharacter.ID !== 0 && CharacterCanChangeToPose(CurrentCharacter, "KneelingSpread")) {
          Pose(CurrentCharacter, "KneelingSpread");
          return;
        }
        if (MouseIn(...buttlegsclosed) && CurrentCharacter.ID !== 0 && CharacterCanChangeToPose(CurrentCharacter, "LegsClosed")) {
          Pose(CurrentCharacter, "LegsClosed");
          return;
        }
        if (MouseIn(...buttspread) && CurrentCharacter.ID !== 0 && CharacterCanChangeToPose(CurrentCharacter, "Spread")) {
          Pose(CurrentCharacter, "Spread");
          return;
        }
        if (MouseIn(...butthogtied) && CurrentCharacter.ID !== 0 && CharacterCanChangeToPose(CurrentCharacter, "Hogtied")) {
          Pose(CurrentCharacter, "Hogtied");
          return;
        }
        if (MouseIn(...buttallfours) && CurrentCharacter.ID !== 0 && CharacterCanChangeToPose(CurrentCharacter, "AllFours")) {
          Pose(CurrentCharacter, "AllFours");
          return;
        }
      }
      
      next(args);
    })
  }
  //catches dialog draw to draw the pose ui buttons
  async function poseMenuOthers() {
    await waitFor(() => !!DialogDraw);
    modApi.hookFunction('DialogDraw', 4, (args,next) => {
      if(CurrentCharacter.ID === 0) {
        SelfMenuDraw();
      }
      if(poseuirender === 1 && poseui === 1 && CurrentCharacter.ID !== 0){
        DrawButton(...buttposeui,"Poses","Green","","EBCH: Pose UI off");
      } else if(poseuirender === 1 & poseui === 0 && CurrentCharacter.ID !== 0) {
        DrawButton(...buttposeui,"Poses","White","","EBCH: Pose UI on");
      }
      
      if(CurrentCharacter.ID !== 0 && poseui === 1) {
        PoseMenuDraw();
      }
      next(args);
    })
  }
  //catches incoming messages for chatlogging
  async function ebchLogging () {
    await waitFor(() => !!ChatRoomMessage);
    modApi.hookFunction("ChatRoomMessage", 4, (args, next) => {
      next(args);
      if(CurrentScreen === "ChatRoom" && Array.from(document.getElementsByClassName('ChatMessage')).slice(-1)[0]) {
        ebchlogadd();
        return;         
        
      }
      return;
    })
  }
  //handles chat commands
    async function crCommands() {
    await waitFor(() => !!CommandCombine);
    
    modCommands = [
      {
        Tag: "ebch",
        Description: "Calls EBCH Help Menu, prefixes most EBCH commands.",
        Action: (self, msg, args, parsed) => {
          let cmd;
          
          //if there is no argument, fill it with help to get the help menu
          if(self === "")
          {
            cmd = "help";
          } else {
            cmd = args.shift();
          }
          let cmd2 = args.shift();
          let cmd3 = args.shift();
          let lcposearray = posearray.map(pose =>{
            return pose.toLowerCase();
          });
          switch (cmd) {
            default: ChatRoomSendLocal(`EBCH: Unknown command. (${cmd})`);
            //help case start
            case "help":
              ChatRoomSendLocal(
                generalhelp
                );
              break;
            // help case end

            case "welcome":
              welcomemsgtoggle = (welcomemsgtoggle === 0) ? 1 : 0;
              ChatRoomSendLocal("EBCH: Welcome message turned " + (welcomemsgtoggle ? "on" : "off") + ".");
              Save();
              break;

            // reset case start
            case "reset":
              ChatRoomSendLocal("EBCH: Resetting to defaults and clearing saved settings.");
              logging = 0;
              notifs = 0;
              ungarble = 0;
              poseui = 0;
              HearingWhitelist = [];
              notifwords = [];
              Save();
            break;
            //reset case end

            case "pose":
              if(cmd2 === undefined)
              {
                cmd2 = "help";
              }
              switch(cmd2){

                default:
                  if(lcposearray.includes(cmd2)){
                    var index = lcposearray.indexOf(cmd2);
                    var ps = posearray[index];
                    var target = targetfind(cmd3);
                    if(target){
                      Pose(target,ps);
                      break;
                    } else {
                      ChatRoomSendLocal(`EBCH: Target couldn't be found in chatroom (${cmd3})`);
                      break;
                    }
                  } else {
                    ChatRoomSendLocal(`EBCH: Unkown Subcommand. (${cmd2})`);
                  }

                case "help":
                  ChatRoomSendLocal(
                  posehelp
                  );
                  break;

                case "on":
                  if(poseuirender === 0){
                    poseuirender = 1;
                    ChatRoomSendLocal("EBCH: Rendering of the pose ui button turned on.");
                    Save();
                  } else {
                    ChatRoomSendLocal("EBCH: Pose UI rendering already on.");
                  }
                  break;

                case "off":
                  if(poseuirender === 1){
                    poseuirender = 0;
                    ChatRoomSendLocal("EBCH: Rendering of the pose ui button turned off.");
                    Save();
                  } else {
                    ChatRoomSendLocal("EBCH: Rendering of the pose UI already off.")
                  }
              }
              break;

            //log case start
            case "log":
              //if there is no second argument, fill it with help instead
              if(cmd2 === undefined)
              {
                cmd2 = "help";
              }
              //begin log switch statement
              switch (cmd2) {
                default: ChatRoomSendLocal(`EBCH: Unknown subcommand. (${cmd2})`);
                

                case "help":
                  ChatRoomSendLocal(
                  loghelp
                  );
                  break;

                case "on":
                  if (logging === 0) {
                    if(dbsetup === 0) {openDb("BCLogs"+ JSON.stringify(Player.MemberNumber), "logs" + JSON.stringify(Player.MemberNumber));}
                    logging = 1;
                    ChatRoomSendLocal("EBCH: Chatlogging turned on.");
                    Save();
                  } else {
                    ChatRoomSendLocal("EBCH: Chatlogging already turned on.");
                  }
                  break;

                case "off":
                  if(logging === 1) {
                    logging = 0;
                    ChatRoomSendLocal("EBCH: Chatlogging turned off.");
                    Save();
                  } else {
                    ChatRoomSendLocal("EBCH: Chatlogging already turned off.");
                  }
                break;

              case "clear":
                ChatRoomSendLocal("EBCH: Attempting to clear database.");
                var store = "logs" + JSON.stringify(Player.MemberNumber);
                clearObjectStore(store);
                break;

              case "download":
                ChatRoomSendLocal("EBCH: Preparing export.");
                saveTextAsFile();
                break;

              
              }
              break;

            case "notifs":
              if(cmd2 === undefined)
              {
                cmd2 = "help";
              }
              switch(cmd2){
                default: ChatRoomSendLocal(`EBCH: Unknown subcommand. (${cmd2})`);

                case "help":
                  ChatRoomSendLocal(
                  notifshelp
                  );
                  break;

                case "on":
                  if (notifs === 0){
                    notifs = 1;
                    ChatRoomSendLocal("EBCH: Custom Notifications turned on.");
                    Save();
                  } else {
                    ChatRoomSendLocal("EBCH: Custom Notifications already on.");
                  }
                  break;

                case "off":
                  if(notifs === 1){
                    notifs = 0;
                    ChatRoomSendLocal("EBCH: Custom Notifications turned off.");
                    Save();
                  } else {
                    ChatRoomSendLocal("EBCH: Custom Notifications already off.");
                  }
                  break;

                case "add":
                  if(cmd3 !== ""){
                    notifwords.push(cmd3);
                    ChatRoomSendLocal("EBCH: Added " + cmd3 + " to the notification words.");
                    Save();
                  } else {
                    ChatRoomSendLocal("EBCH: No word provided as a third argument to add.");
                  }
                  break;

                case "rem":
                  var index = notifwords.indexOf(cmd3);
                  if(index > -1 && cmd3 !== "")
                  {
                    notifwords.splice(index,1);
                    ChatRoomSendLocal("EBCH: Removed " + cmd3 + " from the notification words.");
                    Save();
                  } else if(cmd3 === "") {
                    ChatRoomSendLocal("EBCH: No third argument provided.");
                  } else {
                    ChatRoomSendLocal("EBCH: Word not found in the list.");
                  }
                  break;

                case "clear":
                  notifwords = [];
                  ChatRoomSendLocal("EBCH: Notification Words cleared.");
                  Save();
                  break;

                case "list":
                  ChatRoomSendLocal("EBCH: Notification Words: " + notifwords);
                  break;

                

              }
              break;

            case "ungarble":
              if(cmd2 === undefined)
              {
                cmd2 = "help";
              }
              switch(cmd2){
                default: ChatRoomSendLocal(`EBCH: Unknown subcommand. (${cmd2})`);

                case "help":
                  ChatRoomSendLocal(
                  ungarblehelp
                  );
                  break;

                case "off":
                  if(ungarble !== 0){
                    ungarble = 0;
                    ChatRoomSendLocal("EBCH: Ungarble turned off.");
                    Save();
                  } else {
                    ChatRoomSendLocal("EBCH: Ungarble already off.");
                  }
                  break;

                case "hw":
                  if(ungarble !== 1) {
                    ungarble = 1;
                    ChatRoomSendLocal("EBCH: Ungarble turned on. (Hearing Whitelist mode)");
                    Save();
                  } else {
                    ChatRoomSendLocal("EBCH: Ungarble already on. (Hearing Whitelist Mode)");
                  }
                  break;

                case "all":
                  if(ungarble !== 2){
                    ungarble = 2;
                    ChatRoomSendLocal("EBCH: Ungarble turned on. (All)");
                    Save();
                  } else {
                    ChatRoomSendLocal("EBCH: Ungarble already on. (All)");
                  }
                  break;

                case "add":
                  var target = targetfind(cmd3);
                  if(target !== "" && target !== null && target !== undefined && !HearingWhitelist.includes(target.MemberNumber)) {
                    HearingWhitelist.push(target.MemberNumber);
                    ChatRoomSendLocal("EBCH: Added " + target.Name + " to the hearing whitelist.");
                    Save();
                  } else if(target !== "" || target === null || target === undefined) {
                    ChatRoomSendLocal("EBCH: Target could not be found in chatroom, or no name/number was specified.");
                  } else if(HearingWhitelist.includes(target.MemberNumber)) {
                    ChatRoomSendLocal("EBCH: Provided name/number is already on the whitelist.");
                  } else {
                    ChatRoomSendLocal("EBCH: Unspecified error.");
                  }
                  break;

                case "rem":
                  var n = cmd3;
                  n = parseInt(n);
                  if(HearingWhitelist.includes(n)){
                    var index = HearingWhitelist.indexOf(n);
                    HearingWhitelist.splice(index,1);
                    ChatRoomSendLocal("EBCH: Removed " + cmd3 + " from the hearing whitelist.");
                    Save();
                  } else {
                    ChatRoomSendLocal("EBCH: Could not find number in the hearing whitelist.");
                  }
                  break;

                case "list":
                  ChatRoomSendLocal("EBCH: Hearing Whitelist: " + HearingWhitelist);
                  break;

                case "clear":
                 HearingWhitelist = [];
                 ChatRoomSendLocal("EBCH: Hearing Whitelist Cleared.");
                 break;

                
              }
              break;
          }
        }
      }
    ];
    CommandCombine(modCommands);  
  }

})();
